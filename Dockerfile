FROM ubuntu:trusty

RUN apt-get update && \
    apt-get install mysql-client cron bzip2 -y && \
    mkdir /backups

ENV BACKUP_TIME 0 3 * * *

VOLUME /backups

COPY docker-entrypoint.sh /entrypoint.sh
COPY backup /bin/
RUN chmod +rx /entrypoint.sh
RUN chmod +rx /bin/backup
RUN chmod 777 /bin/backup
RUN chmod 777 /backups
RUN chmod 777 /root/.profile

ENTRYPOINT ["/entrypoint.sh"]

CMD ["cron", "-f"]
