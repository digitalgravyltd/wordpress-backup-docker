#!/bin/bash

env |sed 's/^\(.*\)=\(.*\)$/export \1="\2"/g' >/root/.profile

echo "`date '+%Y-%m-%d--%H-%M'`" >> /backups/backup.log
echo "------------" >> /backups/backup.log
echo "  SQL Details:" >> /backups/backup.log
echo "  user: root" >> /backups/backup.log
echo "  pass: $MYSQL_ROOT_PASSWORD" >> /backups/backup.log
echo "  name: $MYSQL_DB_NAME" >> /backups/backup.log
echo "  ------------" >> /backups/backup.log

echo "  Setting crontab:" >> /backups/backup.log

echo "  Current crontab:" >> /backups/backup.log
crontab -u root -l >> /backups/backup.log
#write out current crontab
crontab -u root -l > mycron
#echo new cron into cron file
echo "$BACKUP_TIME         . /root/.profile; backup >> /backups/backup.log 2> /backups/error.log" >> mycron
#install new cron file
crontab -u root mycron
rm mycron

echo "  New crontab:" >> /backups/backup.log
crontab -u root -l >> /backups/backup.log

echo "  ------------" >> /backups/backup.log

echo "  Backing up now..." >> /backups/backup.log
echo "------------" >> /backups/backup.log
backup >> /backups/backup.log

exec "$@"